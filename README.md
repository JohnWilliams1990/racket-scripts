


Racket Programming Language Practice. 

These scripts are lessons from the Racket Programming book 

'How TO Design Programs, an introduction to Programming and computing'

list2.scm is a recursive circle drawer that draws circles based on a list of numbers. These numbers are generated via recursion 
using the seedn function, which is based on an idea frm Matlab. 

factorial.rkt is a cmdline linux program that executes and recursively computes the factorial of a number. 
