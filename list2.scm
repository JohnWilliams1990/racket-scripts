;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-advanced-reader.ss" "lang")((modname list2) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #t #t none #f () #f)))



(require htdp/draw)

;; function to build a list of numbers
( define (seed l n)
  (cond 
   [ (empty? l ) 
     ( seed (cons 1 '() ) n)
   ]   
   [( not (= (first l) n ) ) 
     ( seed (cons (+ (first l) 1 ) l) n)
   ]   
   [else 
     l   
   ])
)

;; function to build a list of numbers starting at n
;; and increasing by n til m
;; '(n (n+n) (n+n+n) .... m)
( define (seedn l n m)
  (cond 
   [ (empty? l ) 
     ;( seedn (cons 0 '() ) n m)
     ( seedn (cons n '() ) n m)
   ]   
   [( not (>= (first l) m ) ) 
     ( seedn (cons (+ (first l) n ) l) n m)
   ]   
   [else 
     l   
   ])  
)

;; function that takes in a posn and a list of 
;; numbers and draws circles using the list as
;; the list of radii
(define (draw-circles po list-n)
  (cond
    [(empty? list-n)
     #t
    ]
    [(and (posn? po) (number? (first list-n)))
      (and (draw-circles po (rest list-n))
           (sleep-for-a-while .25)
           (draw-circle po (first list-n) 'black))
    ]
  )
)


;(seedn '() 5 300)
;(draw-circles (make-posn 150 150) '(1 10 20 30 40 50 60))
;(draw-circles (make-posn 150 150) (seedn '() 5 300))
(define sizer 700)
(define delta 2)

(start sizer sizer)

(draw-circles (make-posn (/ sizer 2) (/ sizer 2)) (seedn '() delta sizer))
