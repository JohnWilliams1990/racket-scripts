#lang racket

(define-struct ir (name price))

(define (sum-an-inv list-n)
  (cond
    [(empty? list-n)
     0
    ]
    [(number? (ir-price (first list-n)))
      (+ (sum-an-inv (rest list-n)) (ir-price (first list-n)))
    ])
)
(define (price-of list-n name)
  (cond
    [(empty? list-n)
     (error "no item found...")
    ]
    [(equal? (ir-name (first list-n)) name)
      (ir-price (first list-n))
    ]
    [else
      (price-of (rest list-n) name)
    ]
    )
)

(define (extract list-n item )
(cond
    [(empty? list-n)
     (error "no item found...")
    ]
    [(equal? (ir-name (first list-n)) item)
      (first list-n)
    ]
    [else
      (extract (rest list-n) item)
    ]
    )
)
;; extracts all items in list less than one dollar
(define (extract1 list-n)
(cond
    [(empty? list-n)
     empty
    ]
    [(<= (ir-price (first list-n)) 1.00)
      (cons (first list-n) (extract1 (rest list-n))) 
    ]
    [else
      (extract1 (rest list-n))
    ]
    )
)

(define (list-print-ir list-n)
(cond
    [(empty? list-n)
      empty
    ]
    [else
      (display (ir-name (first list-n)))
      (display "\n")
      (display (ir-price (first list-n)))
      (display "\n")
      (list-print-ir (rest list-n)) 
    ])
)
(define shopping-list
  (cons (make-ir 'broccoli 0.99)
  (cons (make-ir 'cheese 3.99)
  (cons (make-ir 'potatoes 2.99)
        
  (cons (make-ir 'gum 0.90)
  (cons (make-ir 'chicken 1.99)
    '()))))
    ))

(sum-an-inv shopping-list)
(price-of shopping-list 'broccoli)
(price-of shopping-list 'chicken)
(price-of shopping-list 'cheese)
(list-print-ir (extract1 shopping-list))


