#lang racket 

(define (sum listn) 
  (cond 
  [(empty? listn)
   0 ; (first listn) 
  ]
  [else 
    (+ (sum (rest listn)) (first listn))
  ]
  )
)

( define (seed l n)

  (cond 
   [ (empty? l )
     ( seed (cons 1 '() ) n)
   ]
   [( not (= (first l) n ) )
     ( seed (cons (+ (first l) 1 ) l) n)
   ]
   [else 
     l
   ])
)

(define (reverse list-n list-r)
  
  (cond 
   [ (empty? list-n )
     list-r
   ]
   [else 
     (reverse (rest list-n) (cons (first list-n) list-r))
   ])
)
(define (length list-n)
  (cond 
    [(empty? list-n) 0]
    [else
      (+ (length (rest list-n)) 1)
    ])
)
(define (numeric? list-n)
  (cond
    [(empty? list-n) #t]
    [(not (number? (first list-n)))
      (and (numeric? (rest list-n)) #f)
    ]
    [else
      (and (numeric? (rest list-n)) #t)
    ])
)
(define (average list-n)
  (cond
    [(numeric? list-n) 
      (/ (sum list-n) (length list-n))
    ]
    [else
      (error "Non number in list")
    ])
)

(define (check-range list-n low high)  
  (cond
    [(empty? list-n) #t]
    [(and (empty? (rest list-n) )
      (or (< (first list-n) low)
         (> (first list-n) high) ))
      ;(=(first list-n) 1)
      #f
    ]
    [(or (< (first list-n) low )
         (> (first list-n) high)     
      ) 
      ;(first list-n)
      #f
    ]
    [(and (>= (first list-n) low )
         (<= (first list-n) high)) 
     (and (check-range ( rest list-n) low high) #t)
    ]  
    [else
      (error "Non num")
    ])
)

;(seed '() 1000 )
;(reverse (seed empty 1000) '())
;(length (seed empty 1000))
(numeric? (cons 'fswef (seed empty 1000)))
( sum (seed '() 10 ))
(average (seed '() 12))

(check-range  '() -11 200)

(check-range (seed '() 100)  2 200)
(check-range (seed '() 100)  1 200)
(check-range (reverse (seed empty 100) '()) 0 200)
